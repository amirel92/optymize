	
var body = document.getElementById('body'),
	modal = document.getElementById('modal'),
	colorSelected = document.getElementById('color-selected'),
	numberRegex = /^([1-9][0-9]+|[1-9])$/

// Show modal on click
document.getElementById('show-modal').onclick = function() {
	showModal();
};
// Hide modal on 'Esc'
window.onkeydown = function(ev) {
	var ev = ev || window.event;
	if (ev.which == 27) {
		closeModal();
	}
};

// Modal show/hide
function showModal() {
	modal.classList.add('active');
	body.classList.remove('overflow');
};
function closeModal() {
	modal.classList.remove('active');
	body.classList.add('overflow');
	window.scrollTo(0,0);
	clearData(input);
	clearData(select);
	clearData(textarea);
	changeBrand();
	colorSelected.style.backgroundColor = color;
};

// Change logo 
function changeBrand() {
	var name = event.currentTarget.value;
	var imgSource = 'img/' + name + '.png';
	document.getElementById('logo-brand').style.backgroundImage = "url("  + imgSource + ")";
}

// Damged description
var descrDamage = document.getElementById('damaged-descr'),
	isDamage = document.getElementById('isDamaged');
function showDamaged() {
	descrDamage.classList.remove('hidden')
}
function hideDamaged() {
	descrDamage.classList.add('hidden')
	descrDamage.classList.remove('error')
}

// verify Mileage to be a number
function verifyMileage() {
	var ev = event.currentTarget;
	if(ev.value.length > 0){
		if (!numberRegex.test(ev.value)) {
			ev.classList.add('error');
		}else{
			ev.classList.remove('error');

		}
	}
}

// Remove class 'error' on focus
var input = document.getElementsByTagName('input'),
	select = document.getElementsByTagName('select'),
	textarea = document.getElementsByTagName('textarea');

removeError(input);
removeError(select);
removeError(textarea);

function removeError(element) {
	for(i=0; i<element.length; i++){
		element[i].onfocus = function() {
			event.currentTarget.classList.remove('error');

		}
	}
}

// Verify data on Save
function verifyData(element){
	for(i=0; i<element.length; i++){
		el = element[i]
		if(el.value == '') {
			el.classList.add('error');
		}
	}
}

// Clear data on closing modal
function clearData(element){
	for(i=0; i<element.length; i++){
		element[i].value = ''
		element[i].classList.remove('error')
	}
}

// Verify selected color
function selectColor(){
	var ev = event.currentTarget,
		color = ev.value;
	ev.nextElementSibling.style.backgroundColor = color;
}

// Verify damage description 
function verifyDamageDescr(){
	if(isDamaged.checked == true){
		descrDamage.classList.add('error')
	}else{
		descrDamage.classList.remove('error');
	}
}

// Verify auto description
var description = document.getElementById('description');
function verifyDescription(){
	if(description.value == ''){
		description.classList.add('error')
	}
	else{
		description.classList.remove('error');
	}

}

// On save button
function saveDetails() {
	event.preventDefault();
	verifyData(input);
	verifyData(select);
	verifyData(description);
	verifyDamageDescr();
	verifyDescription();
	verified = document.getElementsByClassName('error')
	if(verified.length < 1){
		closeModal();
	}
}

// On cancel button
function cancelDetails() {
	event.preventDefault();
	closeModal();
}